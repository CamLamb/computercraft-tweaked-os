print("Cameron's Computer Craft: Tweaked OS")

print("Installing...")

local repo_url = "https://gitlab.com/jafacakes2011/computercraft-tweaked-os/-/raw/master/"

local files = {
    "lua-modules/dkjson.lua",
    "ccctos/casino/admin.lua",
    "ccctos/casino/banner.lua",
    "ccctos/casino/base.lua",
    "ccctos/casino/cashier.lua",
    "ccctos/casino/racer.lua",
    "ccctos/casino/roulette.lua",
    "ccctos/casino/server.lua",
    "ccctos/gps_base.lua",
    "ccctos/main.lua",
    "ccctos/midi.lua",
    "ccctos/reactor_base.lua",
    "ccctos/server.lua",
    "ccctos/turtle_base.lua",
    "ccctos/turtle_virus.lua",
    "ccctos/version.txt",
    "startup.lua",
    "install.lua",
    "update.lua",
    "uninstall.lua",
}

math.randomseed(os.clock()^5)
local cache_bust = math.random() * 100000000000000

for key, file_name in pairs(files) do
    print("Downloading file " .. file_name)
    local install_contents = http.get(repo_url .. file_name .. "?" .. cache_bust)
    local file = fs.open(file_name, "w")
    file.write(install_contents.readAll())
    file.close()
end

print("Successfully installed!")
