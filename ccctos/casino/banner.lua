local server = require "ccctos/casino/server"

local banner = {}

function banner.Startup(id)
    if id =="entrance" then
        local monitors = {peripheral.find("monitor")}
        local monitor_1 = monitors[1]
        local monitor_2 = monitors[2]
        monitor_1.clear()
        monitor_2.clear()
        monitor_1.setCursorPos(1, 1)
        monitor_2.setCursorPos(1, 1)
        monitor_1.setTextScale(4)
        monitor_2.setTextScale(4)
        if server.IsOnline() then
            monitor_1.write("  Welcome to ")
            monitor_2.write("Cam's Casino")
        else
            monitor_1.write("   Casino   ")
            monitor_2.write("   Closed   ")
        end
        os.sleep(10)
        os.reboot()
    end
end

return banner
