local cashier = {}

local server = require "ccctos/casino/server"

local disk_filename = "data.txt"

local monitor = peripheral.find("monitor")

function cashier.Print(text)
    monitor.clear()
    monitor.setCursorPos(1,1)
    local old_input = term.redirect(monitor)
    print(text)
    term.redirect(old_input)
end

function cashier.DepositOrWithdraw()
    monitor.clear()
    monitor.setCursorPos(2, 3)
    monitor.write("Deposit")
    monitor.setCursorPos(11, 3)
    monitor.write("Withdraw")
    local event, side, xPos, yPos = os.pullEvent("monitor_touch")
    if xPos < 11 then
        monitor.clear()
        return "d"
    else
        monitor.clear()
        return "w"
    end
end

function cashier.Refuel()
    turtle.suckDown()
    turtle.refuel()
    turtle.dropDown()
end

function cashier.Startup()
    cashier.Refuel()
    local response = cashier.DepositOrWithdraw()
    if response == "d" then
        cashier.Deposit()
    elseif response == "w" then
        cashier.Withdraw()
    end
    os.sleep(3)
    os.reboot()
end

function cashier.Deposit()
    local card = cashier.LoadCardFromDisk()
    local account = server.account.getFromCard(card)
    cashier.DisplayBalance(account)

    cashier.Print("Please put the items to deposit into the crate. Click when done!")
    os.pullEvent("monitor_touch")
    cashier.Print("Depositing...")
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    turtle.suck()
    cashier.GoToChest()
    local diamond_count = 0
    for i = 1, 16, 1 do
        turtle.select(i)
        local item_detail = turtle.getItemDetail()
        if item_detail ~= nil then
            if item_detail.name == "minecraft:diamond" then
                diamond_count = diamond_count + item_detail.count
                turtle.drop()
            end
        end
    end
    cashier.ReturnFromChest()
    cashier.Print("Deposited " .. diamond_count .. " diamonds.")
    cashier.EmptyInventory()
    account.balance = account.balance + diamond_count
    server.account.update(account)
    cashier.Print("New balance: " .. account.balance)
    disk.eject("left")
end

function cashier.Withdraw()
    local card = cashier.LoadCardFromDisk()
    local account = server.account.getFromCard(card)
    cashier.DisplayBalance(account)
    local amount = cashier.WithdrawAmount(account)
    cashier.Dispense(amount)
    account.balance = account.balance - amount
    server.account.update(account)
    cashier.Print("New balance: " .. account.balance)
    disk.eject("left")
end


function cashier.GetAmount()
    monitor.clear()
    monitor.setCursorPos(1, 1)
    monitor.write("Select amount:")
    monitor.setCursorPos(1, 3)
    monitor.write("-")
    monitor.setCursorPos(18, 3)
    monitor.write("+")
    local amount = 0
    while true do
        monitor.setCursorPos(9, 3)
        monitor.write(tostring(amount) .. " ")
        local event, side, xPos, yPos = os.pullEvent("monitor_touch")
        if xPos >= 15 then
            amount = amount + 1
        elseif xPos <= 4 then
            amount = amount - 1
        else
            print("Breaking")
            break
        end
    end
    cashier.Print("Withdrawing...")
    return amount
end

function cashier.WithdrawAmount(account)
    cashier.Print("Please enter the amount you wish to withdraw from your account:")
    local amount = cashier.GetAmount()
    if amount > account["balance"] then
        cashier.Print("The amount you entered is more than you have available.")
        os.pullEvent("monitor_touch")
        return cashier.WithdrawAmount(account)
    end
    if amount < 0 then
        cashier.Print("The amount you entered is less than zero.")
        os.pullEvent("monitor_touch")
        return cashier.WithdrawAmount(account)
    end
    if amount > 1024 then
        cashier.Print("You can only take out 1024 at a time.")
        os.pullEvent("monitor_touch")
        return cashier.WithdrawAmount(account)
    end
    return amount
end

function cashier.Dispense(amount)
    cashier.Collect(amount)
    cashier.EmptyInventory()
end

function cashier.EmptyInventory()
    for i = 1, 16, 1 do
        turtle.select(i)
        turtle.drop()
    end
end

function cashier.GoToChest()
    turtle.back()
    turtle.back()
    turtle.up()
    turtle.up()
    turtle.turnLeft()
    turtle.turnLeft()
end

function cashier.ReturnFromChest()
    turtle.turnLeft()
    turtle.turnLeft()
    turtle.down()
    turtle.down()
    turtle.forward()
    turtle.forward()
end

function cashier.Collect(amount)
    cashier.GoToChest()
    turtle.suck(amount)
    cashier.ReturnFromChest()
end

function cashier.DisplayBalance(account)
    cashier.Print("Your current balance: " .. account["balance"])
end

function cashier.LoadCardFromDisk()
    local card = {}
    -- Check a disk is in the drive
    if not disk.isPresent("left") then
        cashier.Print("Please enter your membership card into the slot on the right...")
        os.sleep(2)
        return cashier.LoadCardFromDisk()
    end
    local disk_path = disk.getMountPath("left")
    local disk_data = fs.open(disk_path .. "/" .. disk_filename, "r")
    -- Read from disk
    card["id"] = tonumber(disk_data.readLine())
    card["uuid"] = disk_data.readLine()
    disk_data.close()
    local account = server.account.getFromCard(card)
    card["account_id"] = account["id"]
    return card
end

return cashier