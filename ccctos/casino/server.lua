local server = require "ccctos/server"
local json = require "lua-modules/dkjson"
local casino_server = {}
local account = {}
local card = {}

function casino_server.IsOnline()
    local ws = server.GetWebsocket()
    return ws ~= nil
end


function casino_server.sendCommand(command)
    local ws = server.GetWebsocket()
    if ws == nil then
        print("Could not connect to websocket")
        return
    end
    local ws_command = json.encode(command)
    ws.send(ws_command)
    local json_message = ws.receive()
    local obj, pos, err = json.decode(json_message, 1, nil)
    if err then
        print ("Error:", err)
    end
    ws.close()
    return obj
end


function casino_server.receive()
    local ws = server.GetWebsocket()
    if ws == nil then
        print("Could not connect to websocket")
        return
    end
    local json_message = ws.receive()
    local obj, pos, err = json.decode(json_message, 1, nil)
    if err then
        print ("Error:", err)
    end
    return obj
end


function account.new(name)
    -- Trigger bank.NewAccount(name)
    local command = {}
    command["command"] = "new_account"
    command["name"] = name
    local response = casino_server.sendCommand(command)
    if response == nil then
        return
    end
    return response["account"]
end


function account.get(account_id)
    -- Trigger bank.GetAccount(account_id)
    local command = {}
    command["command"] = "get_account"
    command["account_id"] = account_id
    local response = casino_server.sendCommand(command)
    if response == nil then
        return
    end
    return response["account"]
end


function account.update(account)
    -- Trigger bank.UpdateAccount(account)
    local command = {}
    command["command"] = "update_account"
    command["account"] = account
    local response = casino_server.sendCommand(command)
    if response == nil then
        return
    end
    return response["account"]
end


function account.getFromCard(card)
    -- Trigger bank.GetAccountForCard(card)
    local command = {}
    command["command"] = "get_account_from_card"
    command["card"] = card
    local response = casino_server.sendCommand(command)
    if response == nil then
        return
    end
    return response["account"]
end


function card.new(account_id)
    -- Trigger bank.NewCard(account_id)
    local command = {}
    command["command"] = "new_card"
    command["account_id"] = account_id
    local response = casino_server.sendCommand(command)
    if response == nil then
        return
    end
    return response["card"]
end


local roulette_table = {}


function roulette_table.stopped()
    local command = {}
    command["command"] = "roulette_table_stopped"
    casino_server.sendCommand(command)
end


function roulette_table.spinning()
    local command = {}
    command["command"] = "roulette_table_spinning"
    local response = casino_server.sendCommand(command)
    return response["winning_number"]
end


casino_server["account"] = account
casino_server["card"] = card
casino_server["roulette_table"] = roulette_table
return casino_server
