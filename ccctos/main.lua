local ccctos = {}

local repo_url = "https://gitlab.com/jafacakes2011/computercraft-tweaked-os/-/raw/master/"
local version_filename = "ccctos/version.txt"
local install_filename = "install.lua"
local startup_filename = "start.lua"


function ccctos.cache_bust()
    math.randomseed(os.clock()^5)
    return math.random() * 100000000000000
end


function ccctos.version()
    local version_file = fs.open("ccctos/version.txt", "r")
    local version = version_file.readLine()
    version_file.close()
    return version
end


function ccctos.needs_update()
    local current_version = ccctos.version()
    local latest_version_response = http.get(
        repo_url .. version_filename .. "?" .. ccctos.cache_bust()
    )
    if latest_version_response then
        local latest_version = latest_version_response.readAll()
        if current_version ~= latest_version then
            return true
        end
    end
    return false
end


function ccctos.update()
    if ccctos.needs_update() then
        print("Updating CCCTOS")
        local file = fs.open(install_filename, "w")
        file.write(http.get(repo_url .. install_filename).readAll())
        file.close()
        shell.run("install")
        shell.run("reboot")
    end
end


function ccctos.startup()
    local computer_label = os.getComputerLabel()
    if computer_label and string.find(computer_label, "turtle_virus") then
        local turtle_virus = require "ccctos/turtle_virus"
        turtle_virus.startup()
    end
    if computer_label and string.find(computer_label, "reactor") then
        local reactor_base = require "ccctos/reactor_base"
        reactor_base.Startup()
    end
    if computer_label and string.find(computer_label, "casino") then
        local casino_base = require "ccctos/casino/base"
        casino_base.Startup()
    end
    -- if computer_label and string.find(computer_label, "rednet") then
    --     local rednet_test = require "ccctos/rednet_test"
    --     rednet_test.Startup()
    -- end
    if fs.exists(startup_filename) then
        shell.run("start")
    end
end


return ccctos
