local reactor_base = {}

function reactor_base.Startup()
    local max_fuel = 10000000
    local reactor = peripheral.wrap("back")
    local i = 0
    while i < 10 do
        if reactor.getConnected() then
            local energy_percent = reactor.getEnergyStored() / max_fuel
            if energy_percent < 0.2 then
                reactor.setActive(true)
            end
            if energy_percent > 0.8 then
                reactor.setActive(false)
            end
        end
        os.sleep(2)
        i = i + 1
    end
    shell.run("reboot")
end

return reactor_base
