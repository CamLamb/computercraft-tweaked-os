local turtle_base = {}


function turtle_base.Dig(direction)
    if direction == "forward" then
        turtle.dig()
    elseif direction == "up" then
        turtle.digUp()
    elseif direction == "down" then
        turtle.digDown()
    end
end


function turtle_base.GetCurrentDirection()
    local current_pos = vector.new(gps.locate())
    turtle_base.Dig("forward")
    turtle.forward()
    local new_pos = vector.new(gps.locate())
    local position_difference = new_pos - current_pos
    local heading = ""

    if position_difference.x ~= 0 then
        if position_difference.x > 0 then
            heading = "east"
        elseif position_difference.x < 0 then
            heading = "west"
        end
    end
    if position_difference.y ~= 0 then
        if position_difference.y > 0 then
            heading = "south"
        elseif position_difference.y < 0 then
            heading = "north"
        end
    end

    turtle.back()
    return heading
end


function turtle_base.InventoryFull()
    local has_space = false
    for i = 1, 16, 1 do
        if turtle.getItemCount(i) == 0 then
            has_space = true
            break
        end
    end
    return not has_space
end


function turtle_base.RotateTo(heading)
    local current_heading = turtle_base.GetCurrentDirection()
    if heading == "north" then
        if current_heading == "east" then
            turtle.turnLeft()
        elseif current_heading == "west" then
            turtle.turnRight()
        elseif current_heading == "south" then
            turtle.turnLeft()
            turtle.turnLeft()
        end
    elseif heading == "east" then
        if current_heading == "north" then
            turtle.turnRight()
        elseif current_heading == "west" then
            turtle.turnRight()
            turtle.turnRight()
        elseif current_heading == "south" then
            turtle.turnLeft()
        end
    elseif heading == "south" then
        if current_heading == "north" then
            turtle.turnRight()
            turtle.turnRight()
        elseif current_heading == "west" then
            turtle.turnLeft()
        elseif current_heading == "east" then
            turtle.turnRight()
        end
    elseif heading == "west" then
        if current_heading == "north" then
            turtle.turnLeft()
        elseif current_heading == "south" then
            turtle.turnLeft()
        elseif current_heading == "east" then
            turtle.turnRight()
        end
    end
end

function turtle_base.NavigateTo(x, y, z)
    -- Rotate to point North
    turtle_base.RotateTo("north")

    -- Route to new location
    local current_pos = vector.new(gps.locate())
    local end_pos = vector.new(x, y, z)
    local position_difference = end_pos - current_pos

    -- Navigate East/West
    if position_difference.x > 0 then
        -- Face East
        turtle_base.RotateTo("east")
    elseif position_difference.x < 0 then
        -- Face West
        turtle_base.RotateTo("west")
    end
    local x_move = position_difference.x
    while x_move ~= 0 do
        if turtle.detect() then
            turtle_base.Dig("forward")
        end

        turtle.forward()

        if x_move < 0 then
            x_move = x_move + 1
        else
            x_move = x_move - 1
        end
    end

    -- Navigate North/South
    if position_difference.y ~= 0 then
        if position_difference.y > 0 then
            -- Facing South
            turtle_base.RotateTo("south")
        elseif position_difference.y < 0 then
            -- Facing North
            turtle_base.RotateTo("north")
        end
    end
    local y_move = position_difference.y
    while y_move ~= 0 do
        if turtle.detect() then
            turtle_base.Dig("forward")
        end

        turtle.forward()

        if y_move < 0 then
            y_move = y_move + 1
        else
            y_move = y_move - 1
        end
    end

    -- Navigate Up/Down
    local z_move = position_difference.z
    while z_move ~= 0 do
        if z_move < 0 then
            if turtle.detectDown() then
                turtle_base.Dig("down")
            end
            turtle.down()
            z_move = z_move + 1
        else
            if turtle.detectUp() then
                turtle_base.Dig("up")
            end
            turtle.up()
            z_move = z_move - 1
        end
    end

    turtle_base.RotateTo("north")
end

return turtle_base