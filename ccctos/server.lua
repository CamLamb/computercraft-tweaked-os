local server = {}

local ws_address_filename = "ws_add.txt"

function server.GetAddress()
    if not fs.exists(ws_address_filename) then
        print("Please enter the Websocket address:")
        local address = read()
        local ws_address_file = fs.open(ws_address_filename, "w")
        ws_address_file.writeLine(address)
        ws_address_file.close()
    end
    local ws_address_file = fs.open(ws_address_filename, "r")
    local ws_address = ws_address_file.readLine()
    ws_address_file.close()
    return ws_address
end

function server.GetWebsocket()
    local ws, err = http.websocket(server.GetAddress())
    if err then
        print(err)
    elseif ws then
        return ws
    end
end

return server
