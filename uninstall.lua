print("Cameron's Computer Craft: Tweaked OS")
print("Uninstalling...")

local to_delete = {
    "lua-modules",
    "ccctos",
    "startup.lua",
    "install.lua",
    "update.lua",
    "uninstall.lua",
}

for key, name in pairs(to_delete) do
    fs.delete(name)
end

print("Successfully uninstalled!")
